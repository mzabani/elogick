using System;

namespace Elogick
{
	// Taken from resample.h
	public enum MagickFilterTypes {
		UndefinedFilter,
		PointFilter,
		BoxFilter,
		TriangleFilter,
		HermiteFilter,
		HanningFilter,
		HammingFilter,
		BlackmanFilter,
		GaussianFilter,
		QuadraticFilter,
		CubicFilter,
		CatromFilter,
		MitchellFilter,
		JincFilter,
		SincFilter,
		SincFastFilter,
		KaiserFilter,
		WelshFilter,
		ParzenFilter,
		BohmanFilter,
		BartlettFilter,
		LagrangeFilter,
		LanczosFilter,
		LanczosSharpFilter,
		Lanczos2Filter,
		Lanczos2SharpFilter,
		RobidouxFilter,
		SentinelFilter  /* a count of all the filters, not a real filter */
	};
	
	public enum ImageFormat
	{
		Jpeg,
		Png,
		Gif,
		Bmp,
		Tiff,
		Unknown = 3000
	};
	
	// Taken from image.h
	public enum AlphaChannelType
	{
		UndefinedAlphaChannel,
		ActivateAlphaChannel,
		BackgroundAlphaChannel,
		CopyAlphaChannel,
		DeactivateAlphaChannel,
		ExtractAlphaChannel,
		OpaqueAlphaChannel,
		ResetAlphaChannel,  /* deprecated */
		SetAlphaChannel,
		ShapeAlphaChannel,
		TransparentAlphaChannel
	};
	
	// Taken from layer.h
	public enum ImageLayerMethod
	{
		UndefinedLayer,
		CoalesceLayer,
		CompareAnyLayer,
		CompareClearLayer,
		CompareOverlayLayer,
		DisposeLayer,
		OptimizeLayer,
		OptimizeImageLayer,
		OptimizePlusLayer,
		OptimizeTransLayer,
		RemoveDupsLayer,
		RemoveZeroLayer,
		CompositeLayer,
		MergeLayer,
		FlattenLayer,
		MosaicLayer,
		TrimBoundsLayer
	};
}