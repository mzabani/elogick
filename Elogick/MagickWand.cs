using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Elogick
{
	public class MagickWand : IDisposable
	{
		private IntPtr MagickWandPtr;
		internal IntPtr GetWandPtr()
		{
			return MagickWandPtr;
		}

		#region IDisposable implementation
		public void Dispose()
		{
			//TODO: Check if we can call Dispose more than once without problems (we should be able to!)
			WandImports.DestroyMagickWand(MagickWandPtr);
		}
		#endregion
		
		public bool ExtendCanvasAndPositionImage(int width, int height, int left, int top)
		{
			return WandImports.MagickExtentImage(MagickWandPtr, width, height, new IntPtr(-left), new IntPtr(-top));
		}
		
		private unsafe IntPtr GetCurrentIteratorRow(PixelIterator pixIt) {
			IntPtr num_wands = new IntPtr(1);
			IntPtr* pixelRow = PixelIteratorImports.PixelGetCurrentIteratorRow(ref pixIt, ref num_wands);
			
			return *pixelRow;
		}
		
		public unsafe string GetPixelColor(uint x, uint y)
		{
			IntPtr PixItPtr = PixelIteratorImports.NewPixelRegionIterator(MagickWandPtr, new IntPtr(x), new IntPtr(y), new UIntPtr(1), new UIntPtr(1));
			PixelIterator PixIt = (PixelIterator)Marshal.PtrToStructure(PixItPtr, typeof(PixelIterator));

			IntPtr num_wands = new IntPtr(1);
			IntPtr* PixelRow = PixelIteratorImports.PixelGetCurrentIteratorRow(ref PixIt, ref num_wands);
			
			string color = PixelWandImports.PixelGetColorAsString(*PixelRow);
			
			PixelIteratorImports.ClearPixelIterator(ref PixIt);
			
			return color;
		}
		
		public IEnumerable<string> PixelColorIterator() {
			IntPtr pixItPtr = PixelIteratorImports.NewPixelRegionIterator(MagickWandPtr, new IntPtr(1), new IntPtr(1), new UIntPtr(1), new UIntPtr(1));
			PixelIterator pixIt = (PixelIterator)Marshal.PtrToStructure(pixItPtr, typeof(PixelIterator));
			
			while (true)
			{
				IntPtr pixelRow = GetCurrentIteratorRow(pixIt);
				
				if (pixelRow == IntPtr.Zero)
				{
					PixelIteratorImports.ClearPixelIterator(ref pixIt);
					yield break;
				}
				
				yield return PixelWandImports.PixelGetColorAsString(pixelRow);
			}
		}
		
		public int Width {
			get
			{
				return WandImports.MagickGetImageWidth(MagickWandPtr);
			}
		}
		
		public int Height {
			get
			{
				return WandImports.MagickGetImageHeight(MagickWandPtr);
			}
		}
		
		public ImageFormat Format {
			get
			{
				return Utils.TranslateMagickStringToImageFormat(WandImports.MagickGetImageFormat(MagickWandPtr));
			}
		}

		private int _Size;
		public int Size {
			get
			{
				return _Size;
			}
		}
		
		public bool ChangeResolution(double res_x, double res_y)
		{
			return WandImports.MagickSetImageResolution(MagickWandPtr, res_x, res_y);
		}
		
		public bool ChangeTransparencyToColor(string color)
		{
			IntPtr NewColor = PixelWandImports.NewPixelWand();
			PixelWandImports.PixelSetColor(NewColor, color);
			
			IntPtr OldColor = PixelWandImports.NewPixelWand();
			PixelWandImports.PixelSetColor(OldColor, "none");
			
			bool ret = WandImports.MagickOpaquePaintImage(MagickWandPtr, OldColor, NewColor, 0, false);
			
			PixelWandImports.DestroyPixelWand(NewColor);
			PixelWandImports.DestroyPixelWand(OldColor);
			
			return ret;
		}
		
		/// <summary>
		/// Changes the format of your image. If the desired format to be has no alpha channel, any transparent pixels will be painted white
		/// </summary>
		/// <returns>
		/// True for success and false on failure
		/// </returns>
		/// <param name='Format'>
		/// The new format of your image
		/// </param>
		public bool ChangeFormat(ImageFormat Format)
		{
			if (this.Format == Format)
				return true;
			
			// When converting from PNG to JPEG, set white background color
			if ((this.Format == ImageFormat.Png || this.Format == ImageFormat.Gif) && Format != ImageFormat.Png && Format != ImageFormat.Gif)
			{
				ChangeTransparencyToColor("white");
			}
			
			bool ret = WandImports.MagickSetImageFormat(MagickWandPtr, Utils.TranslateImageFormatToMagickString(Format));
			
			return ret;
		}
		
		public bool SetQuality(int quality)
		{
			return WandImports.MagickSetImageCompressionQuality(MagickWandPtr, quality);
		}
		public bool Save(string filename)
		{
			bool ret = WandImports.MagickWriteImage(MagickWandPtr, filename);
			
			return ret;
		}
		
		private bool ReadFromFile(string filename)
		{
			return WandImports.MagickReadImage(MagickWandPtr, filename);
		}
		private bool ReadFromUrl(string url)
		{
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(new Uri(url, UriKind.Absolute));

			WebResponse response = req.GetResponse();
			using (System.IO.Stream str = response.GetResponseStream())
			{
				this.MagickWandPtr = Magick.GetWandPtrFromStream(str, out _Size);
			}

			return true;
		}
		
		#region Constructors
		internal MagickWand(IntPtr MagickWandPtr)
		{
			this.MagickWandPtr = MagickWandPtr;
		}

		public MagickWand(Stream stream)
		{
			this.MagickWandPtr = Magick.GetWandPtrFromStream(stream, out _Size);
		}

		public MagickWand(string filenameOrUrl)
		{
			// What is a url and what is a file path? We should investigate.
			if (System.Text.RegularExpressions.Regex.IsMatch(filenameOrUrl.ToLower(), @"^(https?://)[a-zA-Z0-9]+\..+/.+"))
			{
				ReadFromUrl(filenameOrUrl);
			}
			else
			{
				this.MagickWandPtr = WandImports.NewMagickWand();
				ReadFromFile(filenameOrUrl);
			}
		}
		#endregion
	}
}
