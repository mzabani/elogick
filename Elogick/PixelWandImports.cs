using System;
using System.Runtime.InteropServices;

namespace Elogick
{
	public static class PixelWandImports
	{
		[DllImport("libMagickWand")]
		public static extern IntPtr NewPixelWand();
		
		[DllImport("libMagickWand")]
		public static extern string PixelGetColorAsString(IntPtr PixelWand);
		
		[DllImport("libMagickWand")]
		public static extern void PixelSetAlpha(IntPtr PixelWand, double alpha);
		
		[DllImport("libMagickWand")]
		public static extern IntPtr DestroyPixelWand(IntPtr PixelWand);
		
		[DllImport("libMagickWand")]
		public static extern bool PixelSetColor(IntPtr PixelWand, string color);
	}
}