using System;
using System.Runtime.InteropServices;

namespace Elogick
{
	public static class WandImports
	{
		[DllImport("libMagickWand")]
		public static extern IntPtr NewMagickWand();
		
		[DllImport("libMagickWand")]
		public static extern bool MagickSetImageCompressionQuality(IntPtr MagickWand, int quality);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickExtentImage(IntPtr MagickWand, int width, int height, IntPtr left, IntPtr top);
		
		[DllImport("libMagickWand")]
		public static extern uint ParseChannelOption(string channel);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickSetImageResolution(IntPtr MagickWand, double res_x, double res_y);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickReadImage(IntPtr MagickWand, string filenameOrUrl);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickResizeImage(IntPtr MagickWand, int columns, int rows, MagickFilterTypes FilterTypes, double blur);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickWriteImage(IntPtr MagickWand, string filename);

		[DllImport("libMagickWand")]
		public static extern IntPtr CloneMagickWand(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickSetImageBackgroundColor(IntPtr MagickWand, IntPtr PixelWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickMergeImageLayers(IntPtr MagickWand, ImageLayerMethod PixelWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickGetImagePixelColor(IntPtr MagickWand, int x, int y, IntPtr PixelWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickSetImageAlphaChannel(IntPtr MagickWand, AlphaChannelType alphaType);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickFloodfillPaintImage(IntPtr MagickWand, uint ChannelType, IntPtr fill, double fuzz, IntPtr bordercolor, int x, int y, bool invert);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickTransparentPaintImage(IntPtr MagickWand, IntPtr PixelWandTarget, double alpha, double fuzz, bool invert);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickOpaquePaintImage(IntPtr MagickWand, IntPtr PixelWandTarget, IntPtr PixelWandFill, double fuzz, bool invert);
		
		[DllImport("libMagickWand")]
		public static extern int MagickGetImageHeight(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern int MagickGetImageWidth(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickSetImageFormat(IntPtr MagickWand, string format);
		
		[DllImport("libMagickWand")]
		public static extern string MagickGetImageFormat(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern string MagickGetException(IntPtr MagickWand, IntPtr ExceptionType);
		
		[DllImport("libMagickWand")]
		public static extern int MagickGetExceptionType(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern IntPtr DestroyMagickWand(IntPtr MagickWand);
		
		[DllImport("libMagickWand")]
		public static extern bool MagickCropImage(IntPtr MagickWand, int width, int height, int left, int top);
		
		[DllImport("libMagickWand")]
		public static extern unsafe bool MagickReadImageBlob(IntPtr MagickWand, byte* blob, int length);
		
		[DllImport("libMagickWand")]
		public static extern unsafe bool MagickResetImagePage(IntPtr MagickWand, byte* page);
	}
}