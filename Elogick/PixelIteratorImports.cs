using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Elogick
{
	internal static class PixelIteratorImports
	{
		[DllImport("libMagickWand")]
		internal static extern IntPtr NewPixelRegionIterator(IntPtr MagickWand, IntPtr left, IntPtr top, UIntPtr width, UIntPtr height);
		
		[DllImport("libMagickWand")]
		internal static extern IntPtr ClearPixelIterator(ref PixelIterator PixIt);
		
		[DllImport("libMagickWand")]
		internal static extern void PixelSetFirstIteratorRow(IntPtr PixelIterator);
		
		[DllImport("libMagickWand")]
		internal static extern unsafe IntPtr* PixelGetCurrentIteratorRow(ref PixelIterator PixIt, ref IntPtr number_pixels);
		
		[DllImport("libMagickWand")]
		internal static extern unsafe IntPtr* PixelGetNextIteratorRow(PixelIterator PixIt, int* number_pixels);
	}
	
	/*public static class PixelIterator
	{
		public static unsafe IEnumerable<IntPtr> GetPixelsFromMagickWand(MagickWand Wand)
		{
			int width = Wand.Width;
			int height = Wand.Height;
			IntPtr Iterator = PixelIteratorImports.NewPixelIterator();
			
			IntPtr PixelWand = *PixelIteratorImports.PixelGetCurrentIteratorRow(Iterator, 1);
			yield return PixelWand;
			
			for (int i = 1; i < width * height; i++)
			{
				yield return *PixelIteratorImports.PixelGetNextIteratorRow(Iterator, 1);
			}
		}
	}*/
}