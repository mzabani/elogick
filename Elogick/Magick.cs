using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Elogick
{
	public static class Magick
	{
		// 5MB buffer for stream reading
		private static byte[] StreamBuffer = new byte[5242880];
		private static object StreamBufferLock = new Object();

		internal static unsafe IntPtr GetWandPtrFromStream(Stream stream, out int streamSize)
		{
			IntPtr WandPtr = WandImports.NewMagickWand();
			
			// Avoid doing a lot of work if the image is larger than the buffer (if the Stream is seekable)
			if (stream.CanSeek && stream.Length >= 5242880)
				throw new InsufficientMemoryException("Stream is too large to fit in the image buffer");

			// WARNING
			// This lock is horrible.. arrange a better way later
			lock (StreamBufferLock)
			{
				streamSize = 0;
				do
				{
					int read_byte = stream.ReadByte();
					if (read_byte < 0)
						break;

					StreamBuffer[streamSize] = (byte)read_byte;
					
					streamSize++;
				} while (streamSize < 5242880);
				
				// If we hit the size limit, throw exception
				if (streamSize == 5242880)
					throw new InsufficientMemoryException("Stream is too large to fit in the image buffer");

				// Now create an unmanaged array from the array above
				//TODO:
				// Avoid creating a copy of the array... trying to access our managed array directly
				// from unmanaged code would be better (we still need to fix the managed array, but it shouldn't be
				// a strong hit to the GC since it should stay in the Large Object Space with sgen)
				fixed (byte* blob = new byte[streamSize])
				{
					for (int i = 0; i < streamSize; i++)
					{
						*(blob + i) = StreamBuffer[i];
					}
					
					WandImports.MagickReadImageBlob(WandPtr, blob, streamSize);
				}
			}
			
			return WandPtr;
		}

		internal static unsafe IntPtr GetWandPtrFromStream(Stream stream)
		{
			int trash;
			return GetWandPtrFromStream(stream, out trash);
		}

		/// <summary>
		/// Creates a new MagickWand from a stream <paramref name="stream"/>.
		/// </summary>
		/// <returns>
		/// The created MagickWand.
		/// </returns>
		/// <param name='stream'>
		/// A stream of bytes that represents an image.
		/// </param>
		public static unsafe MagickWand GetImageFromStream(Stream stream)
		{
			return new MagickWand(stream);
		}
		
		public static MagickWand GetImageFromFile(string filename)
		{
			return new MagickWand(filename);
		}
		
		public static MagickWand GetResizedImage(MagickWand Wand, int width, int height)
		{
			IntPtr PtrCopy = WandImports.CloneMagickWand(Wand.GetWandPtr());
			MagickWand ResizedWand = new MagickWand(PtrCopy);
			WandImports.MagickResizeImage(PtrCopy, width, height, MagickFilterTypes.LanczosFilter, 1);
			
			return ResizedWand;
		}
		
		public static unsafe MagickWand CropImage(MagickWand Wand, int width, int height, int left, int top)
		{
			IntPtr PtrCopy = WandImports.CloneMagickWand(Wand.GetWandPtr());
			MagickWand CroppedWand = new MagickWand(PtrCopy);
			
			if (left + width > Wand.Width)
				width = Wand.Width - left;
			
			if (top + height > Wand.Height)
				height = Wand.Height - top;
			
			WandImports.MagickCropImage(PtrCopy, width, height, left, top);
			
			WandImports.MagickResetImagePage(PtrCopy, null);
			
			return CroppedWand;
		}
	}
}