using System;

namespace Elogick
{
	public static class Utils
	{
		public static string TranslateImageFormatToMagickString(ImageFormat Format)
		{
			switch (Format)
			{
				case ImageFormat.Jpeg:
					return "JPEG";
				
				case ImageFormat.Png:
					return "PNG";
				
				case ImageFormat.Gif:
					return "GIF";
				
				case ImageFormat.Bmp:
					return "BMP";
				
				case ImageFormat.Tiff:
					return "TIFF";
				
				default:
					return null;
			}
		}
		
		public static ImageFormat TranslateMagickStringToImageFormat(string MagickString)
		{
			switch (MagickString)
			{
				case "JPEG":
					return ImageFormat.Jpeg;
				
				case "PNG":
					return ImageFormat.Png;
				
				case "BMP":
					return ImageFormat.Bmp;
				
				case "GIF":
					return ImageFormat.Gif;
				
				case "TIFF":
					return ImageFormat.Tiff;
				
				default:
					return ImageFormat.Unknown;
			}
		}
	}
}