using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace Elogick
{
	// Found in magick/geometry.h (where it is called _RectangleInfo and typedef'd to RectangleInfo)
	[StructLayout(LayoutKind.Sequential)]
	public struct RectangleInfo
	{
		//[FieldOffset(0)]
		public IntPtr width;
		
		//[FieldOffset(8)]
		public IntPtr height;
		
		//[FieldOffset(8)]
		public IntPtr x;
		
		//[FieldOffset(16)]
		public IntPtr y;
	}
	
	// Found in wand/pixel-iterator.c (where it is called _PixelIterator and typedef'd later)
	[StructLayout(LayoutKind.Sequential)]
	public struct PixelIterator
	{
		//[FieldOffset(0)]
		public IntPtr id;
		
		// The name field is a 4096 byte char array
		//[FieldOffset(4)]
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4096)]
		//[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4096)]
		public string name;
		
		//[FieldOffset(5000)]
		public IntPtr exception;
		
		//[FieldOffset(8 + 5000)]
		public IntPtr view;
		
		// RectangleInfo structure
		//[FieldOffset(2 * 8 + 5000)]
		public RectangleInfo region;
		
		//[FieldOffset(2 * 8 + 5000 + 24)]
		public bool active;
		
		//[FieldOffset(2 * 8 + 5000 + 24 + 4)]
		public IntPtr y;
		
		//[FieldOffset(3 * 8 + 5000 + 24 + 4)]
		// The field below contains the memory address of a pointer (it is a pointer to a pointer)
		public IntPtr pixel_wands;
		
		//[FieldOffset(4 * 8 + 5000 + 24 + 4)]
		public bool debug;
		
		//[FieldOffset(4 * 8 + 5000 + 24 + 8 + 4)]
		public IntPtr signature;
	}
}